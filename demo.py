# -*- coding: utf-8 -*-
#
# Time-stamp: <Wednesday 2020-06-10 14:26:52 AEST Graham Williams>
#
# Copyright (c) Togaware Pty Ltd. All rights reserved.
# Licensed under GPLv3.
# Author: Graham.Williams@togaware.com
#
# A script to demonstrate StellarGraph doing node classification
# on the Cora academic publications dataset.
#
# ml demo sgcora

# Ensure we are running Python3

import sys
if (sys.version_info < (3, 0)):
    print("MLHub requires python3. Exiting...")
    sys.exit(1)

# Remove all logging/warning information of tensorflow from the
# output.

import warnings
import os
import logging

warnings.simplefilter("ignore", category=FutureWarning)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # FATAL
logging.getLogger('tensorflow').setLevel(logging.FATAL)

# Let's get started.

from mlhub.pkg import mlcat, mlask
from textwrap import fill

mlcat("StellarGraph for Node Classification", """\
Welcome to a demonstration of node classification in a graph knowledge
structure. StellarGraph is used to represent the graph. The sample dataset
is a well known public network dataset known as Cora. It is available from
linqs.soe.ucsc.edu/data and consists of nodes which are academic
publications and edges that link citations. The nodes have been classified 
into seven subject areas as we will see below.

A Graph Convolution Network (GCN) is used to build a classification model
to predict the subject area of a publication based on the graph structure.
This neural network model uses a graph convolution layer which uses the
graph adjacency matrix to learn about a publication's citations.

This demonstration will prepare the dataset, create the GCN layers, and
then train a model and evaluate its performance.
""")

import pandas as pd
#import os

import stellargraph as sg
from stellargraph.mapper import FullBatchNodeGenerator
from stellargraph.layer import GCN

from tensorflow.keras import layers, Model, optimizers, losses#, metrics
from sklearn import preprocessing, model_selection
#from IPython.display import display#, HTML
import networkx as nx 
import matplotlib.pyplot as plt 

dataset = sg.datasets.Cora()

mlask(end="\n")

mlcat("Dataset Description", """\
The dataset, available through the StellarGraph package itself, has been
attached and is ready to be loaded into the StellaGraph data structures.
""")

mlcat("", fill(dataset.description) + "\n")

G, node_subjects = dataset.load()

mlask(end="\n")

mlcat("Graph Shape", """\
We can ask for information about the StellarGraph structure to confirm
it matches the description above.
""")

print(G.info())

mlask(begin="\n")

mlcat("Graph Visualisation", """\
The graph can be visualised, demonstrating that even for a small graph
the visualisations are difficult. Certainly we gain some insight from the 
visualisation. 

Recall that the nodes are academic papers and the edges are citations.
The nodes are coloured according to the subject of the paper.

The computation of the visualisation takes quite a few seconds.
It will pop up shortly.

Once the plot is displayed, type f for fullscreen, and click the magnifying
glass to then use the mouse to zoom in to a rectangle. Typing h will resume
the home view.

Press Ctrl-w to close the window and continue.
""", begin="\n")

NUM_COLOURS = len(node_subjects.value_counts())

val_map = {'Case_Based': 0.0/NUM_COLOURS,
           'Genetic_Algorithms': 1.0/NUM_COLOURS,
           'Neural_Networks': 2.0/NUM_COLOURS,
           'Probabilistic_Methods': 3.0/NUM_COLOURS,
           'Reinforcement_Learning': 4.0/NUM_COLOURS,
           'Rule_Learning': 5.0/NUM_COLOURS,
           'Theory': 6.0/NUM_COLOURS,
}

values = [val_map.get(node, 0.25) for node in node_subjects]

options = {
    'cmap': plt.get_cmap('gist_rainbow'),
    'node_color': values,
    'node_size': 1,
    'width': 0.5,
    'edge_color': 'grey',
}
Nx = sg.StellarGraph.to_networkx(G)
nx.draw(Nx, **options)
plt.show()

mlcat("Subject Distribution", """\
Each publication has a subject attribute, which will be the target of
the classification model. The full dataset has the following distribution
of subjects.
""")

print(node_subjects.value_counts().to_frame())

train_subjects, test_subjects = model_selection.train_test_split(
    node_subjects, train_size=140, test_size=None, stratify=node_subjects
)
val_subjects, test_subjects = model_selection.train_test_split(
    test_subjects, train_size=500, test_size=None, stratify=test_subjects
)

mlask(begin="\n", end="\n")

mlcat("Splitting the Dataset", """\
The dataset set is split into three subsets as usual for building models:
the training set of 140 node labels, the tuning dataset of 500 node labels,
leaving 2068 for the test dataset.

The training dataset has the following subject distribution:
""")

print(train_subjects.value_counts().to_frame())

target_encoding = preprocessing.LabelBinarizer()
train_targets   = target_encoding.fit_transform(train_subjects)
val_targets    = target_encoding.transform(val_subjects)
test_targets    = target_encoding.transform(test_subjects)

mlask(begin="\n", end="\n")

mlcat("Machine Learning Model", """\
A model is now being built. This is the Graph Convolution Network model. For a small
dataset it takes a few seconds.
""")

generator = FullBatchNodeGenerator(G, method="gcn")

train_gen = generator.flow(train_subjects.index, train_targets)

gcn = GCN(
    layer_sizes=[16, 16], activations=["relu", "relu"], generator=generator, dropout=0.5
)

x_inp, x_out = gcn.in_out_tensors()

predictions = layers.Dense(units=train_targets.shape[1], activation="softmax")(x_out)

model = Model(inputs=x_inp, outputs=predictions)
model.compile(
    optimizer=optimizers.Adam(lr=0.01),
    loss=losses.categorical_crossentropy,
    metrics=["acc"],
)

val_gen = generator.flow(val_subjects.index, val_targets)

from tensorflow.keras.callbacks import EarlyStopping

es_callback = EarlyStopping(monitor="val_acc", patience=50, restore_best_weights=True)

history = model.fit(
    train_gen,
    epochs=200,
    validation_data=val_gen,
    verbose=0,
    shuffle=False,  # this should be False, since shuffling data means shuffling the whole graph
    callbacks=[es_callback],
)

mlask(begin="\n", end="\n")

mlcat("Accuracy and Loss", """\
For neural networks the learning happens over a series of so-called epochs. After
each epoch we would expect the accuracy of the model to improve and the loss
to reduce. At some point the curves flatten out and we gain little by performing
any further training.

We will display two plots here, one for the accuracy and the other for the loss.
In each we show the performance measure against both the training dataset
(always expected to show better performance) and the tuning (validation) 
dataset (which should be a less biased performance).

Accuracy is the percentage of observations correctly classified.  The
higher the accuracy the better.  The Loss is a measure of the
difference between the predicted and actual values.  The smaller the
loss the better.

Close the window with Ctrl-w.
""")

sg.utils.plot_history(history, return_figure=True).show()

mlask()

test_gen = generator.flow(test_subjects.index, test_targets)

test_metrics = model.evaluate(test_gen, verbose=0)

mlcat("Test Set Metrics", f"""\
The Test Set is a hold-out dataset, not used in the model building at all,
unlike the training and tuning (validation) datasets. The performance measured
on the test dataset is a unbiased (i.e., more realistic) estimate of the 
performance of the model in general.

For our model the {model.metrics_names[1]}uracy is estimated to be
{round(100*test_metrics[1])}%
and the {model.metrics_names[0]} is estimated to be {round(test_metrics[0], 2)}.
""", begin="\n")

all_nodes = node_subjects.index
all_gen = generator.flow(all_nodes)
all_predictions = model.predict(all_gen)

node_predictions = target_encoding.inverse_transform(all_predictions.squeeze())

mlask(end="\n")

mlcat("Sample Predictions")

df = pd.DataFrame({"Predicted": node_predictions,
                   "Actual": node_subjects,
                   "Correct": node_predictions == node_subjects})
print(df.head(20))

mlask(begin="\n")
